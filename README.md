# Java8
This repository consist Java Programs based on Java-8.

**This project is intended for Java 8 practice program**
It includes following Java-8 introductions
- Lambda
- Function Programming
- Stream API
