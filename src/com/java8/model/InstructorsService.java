package com.java8.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InstructorsService {

    public static List<Instructor> getAll(){
        Instructor instructor1 = new Instructor("Mike", 10, "Software Developer", "M",
                true, Arrays.asList("Java Programming", "C++", "Scala", "Spring boot"));

        Instructor instructor2 = new Instructor("Heyle", 14, "DBA", "F",
                true, Arrays.asList("MySQL", "Data warehouse", "Mongo DB", "No Sql", "Oracle DB"));

        Instructor instructor3 = new Instructor("Jenny", 5, "Engineer", "F",
                false, Arrays.asList("Multi Thread Programming", "Data Structure", "CI/CD"));

        Instructor instructor4 = new Instructor("Gene", 11, "Operation Manager", "M",
                false, Arrays.asList("Jenkins", "Maven", "Gradle", "Ant"));

        Instructor instructor5 = new Instructor("Anthony", 15, "Accountant", "M",
                true, Arrays.asList("Finance", "Retail Banking", "Micro Economics"));

        return Arrays.asList(instructor1, instructor2, instructor3, instructor4, instructor5);
    }
}
