package com.java8.functionalinterface.consumer;

import com.java8.model.Instructor;
import com.java8.model.InstructorsService;

import java.util.List;
import java.util.function.Consumer;

/*
A consumer takes an argument and perform operation
on that argument. It does not return anything.
accept() method is called to perform on operation on Consumer
 */
public class ConsumerExample {
    public static void main(String[] args) {
        Consumer<String> consumer = (x) -> System.out.println(x);

        consumer.accept("i am a consumer");

        List<Instructor> instructors = InstructorsService.getAll();

        System.out.println("---------------------");

        // loop through instructors and print all instructor
        Consumer<Instructor> consumer1 = (i1)-> System.out.println(i1);
        instructors.forEach(consumer1);

        System.out.println("---------------------");

        // loop through instructors and print name of the instructors
        Consumer<Instructor> consumer2 = (i1)-> System.out.println(i1.getName());
        instructors.forEach(consumer2);

        System.out.println("---------------------");

        // use of "andThen() default method of consumer. for chaining of consumer operations
        Consumer<Instructor> consumer3 = (i1)-> System.out.println(" teaches "+ i1.getCourses());
        instructors.forEach(consumer2.andThen(consumer3));

    }

}
