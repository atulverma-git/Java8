package com.java8.functionalinterface.consumer;

import com.java8.model.Instructor;
import com.java8.model.InstructorsService;

import java.util.List;
import java.util.function.BiConsumer;

/* BiConsumers accept two arguments and perform operations.
It does not return any value.
accept() is the abstract method and andThen() is default method.
 */
public class BiConsumerExample {
    public static void main(String[] args) {
        //printing two numbers
        BiConsumer<Integer, Integer> biConsumer = (x,y) -> System.out.println("sum of two numbers: "+ (x+y));
        biConsumer.accept(10, 15);

        List<Instructor> instructors = InstructorsService.getAll();
        System.out.println("----------------");
        // print out name of instructors and gender
        BiConsumer<String, String> biConsumer1 = (x,y) -> System.out.println("name: "+ x + " gender: "+y);
        instructors.forEach(instructor ->
                biConsumer1.accept(instructor.getName(), instructor.getGender())
        );

        System.out.println("----------------");
        // print out name of instructors and courses
        BiConsumer<String, List<String>> biConsumer2 = (name, courses) -> System.out.println(
                "name: "+ name + " teaches courses: "+courses
        );
        instructors.forEach(instructor ->
                biConsumer2.accept(instructor.getName(), instructor.getCourses())
                );

    }
}
