package com.java8.functionalinterface.consumer;

import java.util.function.DoubleConsumer;
import java.util.function.IntConsumer;
import java.util.function.LongConsumer;

public class SpecialConsumer {
    public static void main(String[] args) {
        // IntConsumer is specific to integer param
        IntConsumer intConsumer = (x) -> System.out.println(5+x);
        intConsumer.accept(10);

        System.out.println("----------------");

        // DoubleConsumer is specific to double param
        DoubleConsumer doubleConsumer = (x) -> System.out.println(5+x);
        doubleConsumer.accept(80);

        System.out.println("----------------");

        // LongConsumer is specific to long param
        LongConsumer longConsumer = (x) -> System.out.println(x+100);
        longConsumer.accept(1000);

    }
}
