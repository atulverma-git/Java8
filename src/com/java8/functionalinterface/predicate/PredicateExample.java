package com.java8.functionalinterface.predicate;

import com.java8.model.Instructor;
import com.java8.model.InstructorsService;

import java.util.List;
import java.util.function.Predicate;

/* Predicate are type of Functional interface which accepts one argument
and returns a boolean of expression.
test() is the abstract method.
It has other logical operation methods
and() as logical AND
or() as logical OR
negate() as NOT
equals() as ==
 */
public class PredicateExample {
    public static void main(String[] args) {
        Predicate<Integer> predicate1 = (x) -> x>10;
        System.out.println(predicate1.test(15));

        Predicate<Integer> predicate2 = (x) -> x%2==0;
        System.out.println("Is number even: "+predicate2.test(10));

        // Test logical AND, if number is greater then 10 AND even
        System.out.println("AND Operation: "+predicate1.and(predicate2).test(14));

        // Test logical OR, if number is greater then 10 OR even
        System.out.println("AND Operation: "+predicate1.or(predicate2).test(15));

        // test negate. If number is odd
        System.out.println("is number odd: "+predicate2.negate().test(17));

        System.out.println("--------------------------");
        List<Instructor> instructors = InstructorsService.getAll();

        // all instructor who teaches online
        Predicate<Instructor> predicate3 = (x) -> x.isOnlineCourse();

        instructors.forEach(
                instructor -> System.out.println("teaches online: "+predicate3.test(instructor))
        );
    }
}
