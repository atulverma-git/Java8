package com.java8.lambda;

@FunctionalInterface
public interface IncrementByFiveFuncInterface {
    // abstract method
    public int increment(int a);
}
