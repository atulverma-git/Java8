package com.java8.lambda;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.IntStream;

public class SumOfNumbersUsingCallable {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        int [] intArray = IntStream.rangeClosed(0,5000).toArray();
        int sumOfNumbers = IntStream.rangeClosed(0,5000).sum();

        Callable<Integer> callable1 = () -> {
            int sum = 0;
            for(int i = 0; i < intArray.length/2; i++){
                sum+=i;
            }
            return sum;
        };

        Callable<Integer> callable2 = () -> {
            int sum = 0;
            for(int i = intArray.length/2; i < intArray.length; i++){
                sum+=i;
            }
            return sum;
        };

        ExecutorService executorService = Executors.newFixedThreadPool(2);

        List<Future<Integer>> futures = executorService.invokeAll(Arrays.asList(callable1, callable2));

        int sum = 0;
        for (Future<Integer> future: futures){
            sum = sum + future.get();
        }

        System.out.println("sum of numbers expected: "+ sumOfNumbers);
        System.out.println("sum of numbers actual: "+ sum);
    }
}
