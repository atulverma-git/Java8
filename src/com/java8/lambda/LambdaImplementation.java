package com.java8.lambda;

public class LambdaImplementation {
    public static void main(String[] args) {

        // lambda for IncrementByFiveFuncInterface
        IncrementByFiveFuncInterface incByFive = a -> a+5;

        System.out.println("increment by five: "+ incByFive.increment(7));
    }
}
