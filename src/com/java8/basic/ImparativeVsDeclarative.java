package com.java8.basic;

import java.util.stream.IntStream;

public class ImparativeVsDeclarative {
    public static void main(String[] args) {
        //Imperative ex of print in for loop and print
        System.out.println("Imperative ex of print in for loop and print");
        for (int i=0; i<=5; i++){
            System.out.println("imp loop i = "+i);
        }

        // declarative ex of looping and printing
        System.out.println("declarative ex of looping and printing");
        IntStream intStream1 = IntStream.of(1,2,3,4,5);
        intStream1.forEach(System.out::println);

        //or

        IntStream intStream2 = IntStream.rangeClosed(1,5);
        intStream2.forEach(System.out::println);

        //Imperative ex of calculating sum
        System.out.println("Imperative ex of calculating sum");
        int sum=0;
        for (int i=0; i<=5; i++){
            sum+=i;
        }
        System.out.println("imperative sum = "+sum);

        //declarative ex of calculating sum
        System.out.println("declarative ex of calculating sum");
        int sum1 = IntStream.of(1,2,3,4,5).sum();
        System.out.println("declarative sum = "+sum1);

        int sum2 = IntStream.rangeClosed(1,5).sum();
        System.out.println("declarative sum = "+sum2);
    }
}
